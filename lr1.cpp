#include <iostream>
#include <stdio.h>
#include <string.h>

//#define Nalph 256

//using namespace std;

char *caesar_ascii(int, char *, int);
char *vigenere_ascii(int, char *, char *);
char *caesar(int, char *, int, char *);
char *vigenere(int, char *, int *, int, char*);
int my_atoi(char *);
int number_in_alphabet(char, char *);

//STROKA -c/-v -cd/-dc n/codword [aphabet_file]
int main(int argc, char *argv[])
{
setlocale( LC_ALL,"Russian" );

	int code=0, ceas=0, alph=0;
	char *rez;
	FILE *afile;
	if (argc>=5&&argc<=6)
	{
		if (argv[3][0]=='-'&&argv[3][1]=='c'&&argv[3][2]=='d')
			code=2;
		else if (argv[3][0]=='-'&&argv[3][1]=='d'&&argv[3][2]=='c')
			code=1;
		else code=0;

		if ((argv[2][0]=='-'&&argv[2][1]=='c')&&(my_atoi(argv[4])<256))
			ceas=1;
		else if (argv[2][0]=='-'&&argv[2][1]=='v')
			ceas=2;
		else ceas=0;

		if (argc==6)
		{
			afile=fopen(argv[5], "r");
			if (afile!=NULL) alph=1;
			else { alph=0; printf("Wrong Alphabet file!\n"); return -1;}
		}
		else alph=2;//ASCII 
	}


	if (code*ceas*alph>0)
	{
		rez=new char[strlen(argv[1])+1];
		if (alph==1)
		{
			//FILE *afile;
			int Nalph, i;
			char alphabet[256];			
			fscanf(afile, "%s", alphabet);
			//printf("A = %s\n", alphabet);
			//Nalph=strlen(alphabet);
			if (ceas==1)
				rez = caesar(code-1, argv[1], my_atoi(argv[4]), alphabet);
			else
			{
				int *codword, l=strlen(argv[4]);
				bool wrong;
				codword=new int[l];
				for (i=0; i<l; i++)
				{
					codword[i]=number_in_alphabet(argv[4][i], alphabet);
					if (codword[i]<0)
					{
						printf("Wrong symbol in codword!\n");
						return -2;
					}
				}
				
				rez = vigenere(code-1, argv[1], codword, l, alphabet);
			}
		}
		else if (alph==2)	
		{
			if (ceas==1)
				rez = caesar_ascii(code-1, argv[1], my_atoi(argv[4]));
			else
				rez = vigenere_ascii(code-1, argv[1], argv[4]);
		}

		printf("%s\n", rez);
		//system("pause");
		return 0;
	}

	printf("Wrong arguments!\n");
	//system("pause");
	return 1;
}

int my_atoi(char *str)
{
	int i, len, res=0, tmp=1;
	len=strlen(str);
	for (int i=1; i<=len; i++)
	{
		if (str[len-i]>='0'&&str[len-i]<='9')
			res+=(str[len-i]-'0')*tmp;
		tmp*=10;
	}
	if (str[0]=='-') res*=(-1);
	return res;
}

char *caesar_ascii(int code, char *str, int poz)
{
	int i, k, Nalph=256;
	char *res;
	k=strlen(str);
	res = new char[k+1];
	if (code)
		for (i=0; i<k; i++)
			res[i]=(str[i]+poz)%Nalph;
	else 
		for (i=0; i<k; i++)
			res[i]=(str[i]-poz+Nalph)%Nalph;
	res[k]='\0';
	return res;
}

int number_in_alphabet(char c, char *alph)
{
	bool wrong=true;
	int j, Nalph=strlen(alph);
	for (j=0; j<Nalph; j++)
		if (c==alph[j]) 
		{
			wrong=false;
			break;
		}
	if (wrong) return -1;
	else return j;
}

char *caesar(int code, char *str, int poz, char *alph)
{
	int i, j, k, Nalph;
	char *res;
	bool wrong;
	k=strlen(str);
	Nalph=strlen(alph);
	res = new char[k+1];
	if (code)
		for (i=0; i<k; i++)
		{
			j=number_in_alphabet(str[i], alph);
			if (j<0) 
			{	printf("Wrong symbol in your word!\n"); 
				res[i]='\0'; 
				return res;
			}
			res[i]=alph[(j+poz)%Nalph];
		}
	else 
		for (i=0; i<k; i++)
		{
			j=number_in_alphabet(str[i], alph);
			if (j<0) 
			{	printf("Wrong symbol in your word!\n"); 
				res[i]='\0'; 
				return res;
			}
			res[i]=alph[(j-poz+Nalph)%Nalph];
		}
	res[k]='\0';
	return res;
}

char *vigenere_ascii(int code, char *str, char *codword)
{
	char *res;
	int k, l, i; 
	k=strlen(str);
	res=new char[k+1];
	l=strlen(codword);
	if (code)
		for (i=0; i<k; i++)
			res[i]=(str[i]+codword[i%l])%256;
	else 
		for (i=0; i<k; i++)
			res[i]=(str[i]-codword[i%l]+256)%256;
	res[k]='\0';
	return res;
}

char *vigenere(int code, char *str, int *codword, int lcw, char *alph)
{
	char *res;
	bool wrong;
	int k, l, i, j, Nalph;
	Nalph=strlen(alph);
	k=strlen(str);
	res=new char[k+1];
	//l=strlen(codword);lcw
	if (code)
		for (i=0; i<k; i++)
		{
			j=number_in_alphabet(str[i], alph);
			if (j<0)
			{	printf("Wrong symbol in your word!\n"); 
				res[i]='\0'; 
				return res;
			}
	
			res[i]=alph[(j+codword[i%lcw])%Nalph];
		}
	else 
		for (i=0; i<k; i++)
		{
			j=number_in_alphabet(str[i], alph);
			if (j<0)
			{	printf("Wrong symbol in your word!\n"); 
				res[i]='\0'; 
				return res;
			}
	
			res[i]=alph[(j-codword[i%lcw]+Nalph)%Nalph];
		}
	res[k]='\0';
	return res;
}
